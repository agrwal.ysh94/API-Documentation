# API Documentation

## User Management System

1.  
    - API Name: User Registration API
    - URL: /v1/user/register/
    - Module: User
    - Method: POST
    - Header: Platform-Header
    - Request:

        ```json 
        {
    	    "user_name":"N16verma@",
    	    "email":"abcdefg@gmail.com",
    	    "password":"Qwerty@123"
        }
        ```
    
        - Response: __EXCEPTIONED_GENERIC_STATUS_RESPONSE_FORMAT__
            - __HTTP/1.1 200 OK__

        ```json
        {
            "meta": "",
            "data": {
                "profile": {
                    "user_name": "N16verma@",
                    "secret_key": "5RHAQBFQ447PUKY5",
                    "user_id": 4,
                    "email": "abcdefg@gmail.com"
                }
            }
        }
        ```
        
        - __HTTP/1.1 404 Not Found__: Use Generic Response
        
        - __HTTP/1.1 400 Bad Request__: 
            - If a user with that user_name already exists in the database or 
                it does not contain atleast one number or atleast one special char
                or atleast one upper case letter or atleast one lower case letter
                or contains whitespace at starting or in the end.

            ```json
            {
                "meta":"",
                "data":{
                    "errors": "The user name is either taken or does not fits in criteria"
                }
            }
            ```
            
            - If a password does not contain atleast one number or atleast one special char
                or atleast one upper case letter or atleast one lower case letter
                or contains whitespace at starting or in the end.

            ```json
            {
                "meta":"",
                "data":{
                    "errors": "The password does not fits in the criteria"
                }
            }
            ```

2. 
    - API Name: User Login API
    - URL: /v1/user/login/
    - Module: User
    - Method: POST
    - Header: Platform-Header
    - Request:

    ```json
    {
        "user_name":"N16verma@",
        "password":"Qwerty@123",
        "otp_key": "123456"
    }
    ```
    - Response: __EXCEPTIONED_GENERIC_STATUS_RESPONSE_FORMAT__
        - __HTTP/1.1 200 OK__ 

        ```json
        {
            "meta": "",
            "data": {
                "permissions": null,
                "user_id": 4,
                "session_token": "User_4_KgPX4zMWS2gTWbptyPA1nIY5cVpfLf8S"
            }
        }
        ```
        - __HTTP/1.1 404 Not Found__: Use Generic Response
        - __HTTP/1.1 400 Bad Request__: 
            - If the user_name is not registered

            ```json
            {
                "meta": "",
                "data": {
                    "error": [
                        "This User_name is not registred"
                    ]
                }
            }
            ```
            - If the otp_key is wrong

            ```json
            {
                "meta": "",
                "data": {
                    "error": [
                        "The OTP is incorrect"
                    ]
                }
            }
            ```
            -If the password is wrong
            
            ```json
            {
                "meta": "",
                "data": {
                    "error": [
                        "The password is incorrect"
                    ]
                }
            }
            ```
3. 
    - API Name: User Logout API
    - URL: /v1/user/logout/
    - Method: POST
    - Header: Loggedin-Platform-Header
    - Request:
    
        ```json
        {
            "user_id": 4
        }
        ```
    - Response: __ALL_GENERIC_STATUS_RESPONSE_FORMAT__
        
        -HTTP 204_NO_CONTENT : 
        
        ```json
        {
            "meta": "",
            "data": {}
        }
        ```